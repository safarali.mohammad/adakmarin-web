enum ServiceImgEnum {
    'khdm-t-hml-o-nkl-dr' = 'see',
    'khdm-t-hml-ho' = 'air',
    'khdm-t-hml-r-l' = 'train',
    'khdm-t-hml-zm-n-d-khl-o-kh-rg' = 'earth',
    'khdm-t-trm-n-l-h-nt-nr-o-lgst' = 'terminal',
    'khdm-t-bsth-bnd' = 'pack',
    'khdm-t-mr-o-trkh-s-r' = 'gomrok',
    'khdm-t-hml-m-aa-t-tost-fl-s-t-n' = 'liquid',
}
export default ServiceImgEnum