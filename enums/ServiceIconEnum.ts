enum ServiceIconEnum {
    'khdm-t-hml-o-nkl-dr' = 'tsunami',
    'khdm-t-hml-ho' = 'airplane-engines',
    'khdm-t-hml-r-l' = 'train-front',
    'khdm-t-hml-zm-n-d-khl-o-kh-rg' = 'globe',
    'khdm-t-trm-n-l-h-nt-nr-o-lgst' = 'clipboard-check',
    'khdm-t-bsth-bnd' = 'box-seam',
    'khdm-t-mr-o-trkh-s-r' = 'boxes',
    'khdm-t-hml-m-aa-t-tost-fl-s-t-n' = 'droplet',
    'nm-nd-dr-n-d' = 'globe-americas',
    'nm-nd-sht-r-n-dr-m-r-t' = 'geo-alt',

}
export default ServiceIconEnum