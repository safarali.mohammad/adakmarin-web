import { ref } from 'vue'
import { defineStore } from 'pinia'
import api from '@/api/api';

export const useServicesStore = defineStore('getServices', () => {
    const services = ref([])
    const homeServices = ref([])

    const loading = ref(false)

    const getServices = async () => {
        loading.value = true
        await api.get('services').then(res => {
            services.value = res?.data?.data
            loading.value = false
        })
        homeServices.value = [...services.value].splice(0, 8)
    }
    getServices()


    return { services, loading, homeServices }
})