// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['/assets/global.scss',
    'vue-skeletor/dist/vue-skeletor.css',
    'vuetify/lib/styles/main.sass'],
  build: {
    transpile: ['vuetify'],
  
  },
  modules: ["nuxt-bootstrap-icons"],
  bootstrapIcons: {
    renderType: "inline",
  },
  runtimeConfig: {
    public: {
      pageTitle: 'Pacific-Holding | '
    }
  },
  devtools: { enabled: true },
})
