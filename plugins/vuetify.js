import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { fa } from 'vuetify/locale'

export default defineNuxtPlugin(nuxtApp => {
    const vuetify = createVuetify({
        ssr: true,
        locale: {
            locale: 'fa',
            fallback: 'fa',
            messages: { fa },
            rtl: { fa: true },
        },
        components,
        directives,
    })

    nuxtApp.vueApp.use(vuetify)
})
