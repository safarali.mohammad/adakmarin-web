import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api.pacificholding.co/api/v1/',
})

export default instance;